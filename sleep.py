import subprocess
import time
from subprocess import Popen, PIPE, STDOUT


def hybr():
    print("Через сколько перевести в ждущий режим(например: '0.5' -через пол часа, '2' - через 2 часа):")
    time_sleep = float(input()) * 3600
    time.sleep(time_sleep)
    p = subprocess.Popen(["sudo", "-S", "pm-suspend"], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
    grep_stdout = p.communicate(input=b'your_root_pass\n')[0]
    print(grep_stdout.decode())

if __name__ == "__main__":
    hybr()